#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <limits.h>

#define MAX_FILES 1000
#define MAX_DIRS 1000

int depth = 0;
struct FileInfo {
    char name[256];
    ino_t inode;
    off_t size;
    uid_t owner_uid;
    gid_t group_gid;
    mode_t permissions;
    time_t last_accessed;
    time_t last_modified;
};

struct FileInfo Oldinfo[MAX_FILES];
int numFiles = 0;

typedef struct {
    char name[256];
    ino_t inode;
    size_t size;
    time_t modified;
    time_t access;
} Metadata;

typedef struct {
    Metadata files[MAX_FILES];
    int count ;
} Snapshot;


Snapshot snapshot[1000]={0};

void metadata(char director[25],int file, const char *pathname, struct stat *fileInfo, const char *name, char snapshotName[1000], int depth, char *outputDir) {
    char *dotPos = strrchr(name, '.');
    char *extension = NULL; // for saving the extension
    
    if (dotPos != NULL) {
        extension = dotPos + 1;
        *dotPos = '\0'; // remove the extension
    }

    sprintf(snapshotName, "%s_snapshot.%s", name, extension); // create the snapshot file name
    if (snapshotName != NULL && strstr(snapshotName, ".DS_Store") != NULL) 
        return;

    // check if snapshot file already exists in output directory
    char outputSnapshotName[1000];
    sprintf(outputSnapshotName, "%s/%s", outputDir, snapshotName);

    printf("access:%d\n", access(outputSnapshotName, F_OK));

    if (access(outputSnapshotName, F_OK) == 0){
        int i = 0;
        // snapshot file exists, compare it with the current file
        printf("%s\narray:%llu\nactual:%llu\n\n", snapshot->files[i].name,snapshot->files[i].inode, fileInfo->st_ino);
        if ((snapshot->files[i].inode) == (fileInfo->st_ino)){ // we are taking the snapshot of the specified original file
            //printf("changes need to be made!!! %s.\n", name);
            if (snapshot->files[i].size != fileInfo->st_size ||
                snapshot->files[i].modified != fileInfo->st_mtime ||
                snapshot->files[i].access != fileInfo->st_atime){
                //printf("changes need to be made!!! %s.\n", name);
                //update
                return; // No need to create a new snapshot
            }
        }
        i++;
    }
        sprintf(outputSnapshotName, "%s/%s", outputDir, snapshotName); // set the full path for the snapshot file in the output directory

        // Snapshot file does not exist, create a new one
        int snapshot_fd = creat(outputSnapshotName, fileInfo->st_mode); // create a snapshot with the same permissions from the original file
        if (snapshot_fd == -1) {
            perror("Unable to create snapshot file");
            return;
        }

        dprintf(snapshot_fd, "%s\n", name);
        dprintf(snapshot_fd, "    Inode: %llu\n", fileInfo->st_ino);
        dprintf(snapshot_fd, "    Size: %lld bytes\n", (long long)fileInfo->st_size);
        dprintf(snapshot_fd, "    Owner UID: %d\n", fileInfo->st_uid);
        dprintf(snapshot_fd, "    Group GID: %d\n", fileInfo->st_gid);
        dprintf(snapshot_fd, "    Permissions: %o\n", fileInfo->st_mode & 0777);
        dprintf(snapshot_fd, "    Last accessed: %s", ctime(&fileInfo->st_atime));
        dprintf(snapshot_fd, "    Last modified: %s", ctime(&fileInfo->st_mtime));

        strcpy(snapshot->files[snapshot->count].name, name);
        snapshot->files[snapshot->count].inode = fileInfo->st_ino;
        snapshot->files[snapshot->count].size = fileInfo->st_size;
        snapshot->files[snapshot->count].modified = fileInfo->st_mtime;
        snapshot->files[snapshot->count].access = fileInfo->st_atime;

        snapshot->count++;

        char *newSnapshotN = strstr(snapshotName, "/");
        if (newSnapshotN != NULL) {
            newSnapshotN++; // move the pointer to the desired part of the name
        }

        close(snapshot_fd);

        printf("Snapshot for Directory %s created successfully.\n", director);
}

void checkPermissionsAndAnalyze(const char *filePath, struct stat *fileInfo, char *outputDir, char *isolatedSpaceDir, int write_fd) {
    int corruptedFiles = 0;
    //checking permissions
    if (((fileInfo->st_mode & S_IRUSR) == S_IRUSR) &&
        ((fileInfo->st_mode & S_IWUSR) == S_IWUSR) &&
        ((fileInfo->st_mode & S_IRGRP) == 0) &&
        ((fileInfo->st_mode & S_IWGRP) == 0) &&
        ((fileInfo->st_mode & S_IROTH) == 0) &&
        ((fileInfo->st_mode & S_IWOTH) == 0)) {
        int fd[2];
        if (pipe(fd) == -1) {
            perror("pipe");
            exit(EXIT_FAILURE);
        }

        pid_t pid = fork(); // grandchild
        if (pid == -1) {
            perror("fork");
            exit(EXIT_FAILURE);
        } else if (pid == 0) { // it's a child process
            // close read end of pipe 
            close(fd[0]);

            // redirect stdout to pipe
            dup2(fd[1], STDOUT_FILENO);
            // close the original file descriptor associated with the write end of the pipe
            close(fd[1]);

            execl("./verify_for_malicious.sh", "verify_for_malicious.sh", filePath, outputDir, isolatedSpaceDir, NULL);

        } else { // parent process
            close(fd[1]); // close write end of pipe

            // read from the read end of the pipe and print the output
            char result[50];
            ssize_t n = read(fd[0], result, sizeof(result));

            if (n == -1) {
                perror("read");
                exit(EXIT_FAILURE);
            } else if (n > 0) {
                result[n] = '\0';
                //printf("hello world!111\n");
                if (strcmp(result, "SAFE") == 0)
                    //printf("Result for %s: %s\n", filePath, result);
                    printf("SAFE\n");
                else if (strcmp(result, "DANGEROUS") == 0){
                    corruptedFiles++;
                    char *file_name = strrchr(filePath, '/');
                    printf("%s\n", file_name);
                }
                //printf("countCorr %d\n", corruptedFiles);
                // Wwite the corrupted files count to the pipe
                if (write(write_fd, &corruptedFiles, sizeof(int)) == -1) {
                    perror("Write to pipe failed");
                    exit(EXIT_FAILURE);
                }
                exit(corruptedFiles);
            }
            close(fd[0]); // close the read end of pipe
            
            
        }
    }
}


void func( char director[25], DIR *dir, int depth,char *outputDir,char *isolatedSpaceDir, int write_fd){
    struct dirent *entry;
    struct stat fileInfo;
    while ((entry = readdir(dir)) != NULL){
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        char filePath[1000];
        sprintf(filePath, "%s/%s", director, entry->d_name);
        
        int st = lstat(filePath, &fileInfo);
        if(st != -1){
            if (S_ISDIR(fileInfo.st_mode)){
                DIR *sub_dir = opendir(filePath);
                if (sub_dir != NULL) {
                    func( filePath, sub_dir, depth + 1, outputDir,isolatedSpaceDir,write_fd);
                    closedir(sub_dir);
                }
            } else{
                char snapshotName[1000];
                checkPermissionsAndAnalyze(filePath, &fileInfo, outputDir, isolatedSpaceDir, write_fd); 
                metadata(director, 0, filePath, &fileInfo, entry->d_name, snapshotName, depth, outputDir);

            }
            
        }
    }
}

int main(int argc, char *argv[]){
    char *outputDir;
    char *isolatedSpaceDir = NULL;
    int fd[2];
    
    for (int i = 1; i < argc; i++){
        if(strcmp(argv[i],"-o")==0 && i+1<argc){
            outputDir = argv[i + 1];
            i++;
        }else if(strcmp(argv[i],"-s")==0 && i+1<argc){
            isolatedSpaceDir = argv[i + 1];
            i++;
        }
        else{
            DIR *dir;
            if (pipe(fd) == -1) {
            perror("pipe");
            exit(EXIT_FAILURE);
        }
            dir = opendir(argv[i]);
            if (dir == NULL){
                continue;
            }
        
            pid_t pid = fork();
            if (pid == -1) {
                perror("fork failed");
                exit(1);
            } else if (pid == 0) { 
                close(fd[0]); // close read end of pipe in child
                func(argv[i], dir, 0, outputDir, isolatedSpaceDir, fd[1]); // Pass write end of pipe
                closedir(dir);
                close(fd[1]);
                exit(0);
            }
            else {
                close(fd[1]);
                int status;
                pid_t wpid;
                static int i = 1;
                waitpid(pid, &status, 0);
                int corrupted_files;
                read(fd[0], &corrupted_files, sizeof(int)); // Read corrupted files count from pipe
                printf("Child process %d with PID %d has ended with code %d.", i, pid, WEXITSTATUS(status));
                printf(" Corrupted files: %d\n", corrupted_files);
                i++;
                close(fd[0]); 

            }
        }
    }
    return 0;
}




/*
void saveMetadata(const char *name, struct stat *fileInfo) {
    
        struct FileInfo info;
        snprintf(info.name, sizeof(info.name), "%s", name);
        info.inode = fileInfo->st_ino;
        info.size = fileInfo->st_size;
        info.owner_uid = fileInfo->st_uid;
        info.group_gid = fileInfo->st_gid;
        info.permissions = fileInfo->st_mode & 0777;
        info.last_accessed = fileInfo->st_atime;
        info.last_modified = fileInfo->st_mtime;

}
void compareAndUpdateSnapshot(const char *snapshotName, struct stat *snapshotInfo) {
    // Open the snapshot file for reading and writing
    int snapshot_fd = open(snapshotName, O_RDWR);
    if (snapshot_fd == -1) {
        perror("Unable to open snapshot file for update");
        return;
    }

    // Read existing snapshot data
    char buffer[1024];
    ssize_t bytes_read;
    size_t total_bytes_read = 0;
    while ((bytes_read = read(snapshot_fd, buffer + total_bytes_read, sizeof(buffer) - total_bytes_read)) > 0) {
        total_bytes_read += bytes_read;
        if (total_bytes_read >= sizeof(buffer)) {
            // Buffer is full, break to avoid overflow
            break;
        }
    }
    if (bytes_read == -1) {
        perror("Error reading snapshot file");
        close(snapshot_fd);
        return;
    }

    // Parse the existing snapshot data
    char *line = strtok(buffer, "\n");
    while (line != NULL) {
        // Check if the line contains the time accessed information
        if (strstr(line, "Last accessed: ") != NULL) {
            // Update the time accessed information
            char *accessed_time_str = strchr(line, ':') + 2; // Skip the space after ':'
            time_t current_accessed_time = snapshotInfo->st_atime;
            time_t new_accessed_time = atol(accessed_time_str);
            if (current_accessed_time != new_accessed_time) {
                // Time accessed changed, update the line
                lseek(snapshot_fd, -(strlen(accessed_time_str) + 1), SEEK_CUR);
                dprintf(snapshot_fd, "    Last accessed: %s", ctime(&current_accessed_time));
                printf("Snapshot time accessed updated.\n");
            }
            // No need to continue parsing
            break;
        }
        line = strtok(NULL, "\n");
    }

    // Close the snapshot file
    close(snapshot_fd);
}

*/
